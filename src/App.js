import React, { Component } from 'react';
import './App.css';
import Restaurants from './components/Restaurants';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {value:''};
    this.state = {
      restaurants : []
    }
  }

  getDataFromServer() {
    console.log("--- GETTING DATA ---");
    fetch('http://localhost:8080/api/restaurants')
    .then(response => {
        console.log(response);
        return response.json();
    })
    .then(data => { 
      console.log(data);
        let newRestaurants = [];
        data.data.forEach((el) => {
        newRestaurants.push(el);
    });
        this.setState({
            restaurants: newRestaurants
        });

    }).catch(err => {
        console.log("erreur dans le get : " + err)
    });
  }

  componentWillMount() {
    console.log("Component will mount");
    this.getDataFromServer();
  }

  render() {
    let listAvecComponent =
      this.state.restaurants.map((el, index) => {
        return <Restaurants
          id={el._id}
          name={el.name}
          cuisine={el.cuisine}
          />
      });
    return (
      <div className="App">
        <header>

        </header>
        <h1>Liste Restaurants</h1>
        <table><tbody>
                {listAvecComponent}
                </tbody></table>
      </div>
    );
  }
}

export default App;
