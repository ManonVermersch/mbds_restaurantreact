import React, { Component } from 'react';

function Restaurants(props) {
    return (
        <tr id={props.id}>
                        <td>{props.name}</td>
                        <td>{props.cuisine}</td>
                        <td>
                            <button class="btn btn-dark" ><i class="fa fa-edit"></i></button>
                            <button class="btn btn-dark" ><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
    );
  };

  export default Restaurants;